from . import views
from django.urls import path

urlpatterns = [
    path('', views.index),
    path('<str:url_id>/', views.redirect_url),
]
