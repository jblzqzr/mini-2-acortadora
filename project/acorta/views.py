from django.shortcuts import render, redirect
from django.core.exceptions import MultipleObjectsReturned
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError
from .models import ShortenedsUrl


# Create your views here.
@csrf_exempt
def index(request):
    if request.method == 'POST':
        return do_post(request)

    content_list = ShortenedsUrl.objects.all()
    return render(request, 'acorta/index.html', {'content_list': content_list})


def redirect_url(request,url_id):
    try:
        url = ShortenedsUrl.objects.get(short=url_id).url
        print("HOLA" + url)
        return redirect(url)
    except ShortenedsUrl.DoesNotExist:
        return HttpResponse('Invalid Request. URL not found')


def do_post(request):
    request_data = request.POST
    if len(request_data) != 2:
        return HttpResponseBadRequest('Invalid Request. Error in number of parameters')
    try:
        url = request_data['url']
        short = request_data['short']
    except MultiValueDictKeyError:
        return HttpResponseBadRequest('Invalid Request. One of the parameters not found. Be sure to send url and short')

    short, manual, exist = treat_short(short)
    url, ok = treat_url(url)
    c = ShortenedsUrl.objects.filter(url=url).exists()
    if not exist:
        if c:
            try:
                check_manual = ShortenedsUrl.objects.get(url=url).manual_added
            except MultipleObjectsReturned:
                return HttpResponse('Recurso acortado manualmente, no se puede modificar')

        else:
            check_manual = not manual
        if ok and (check_manual != manual):
            url_object = ShortenedsUrl(url=url, short=short, manual_added=manual)
            url_object.save()
            return render(request, 'acorta/lista.html', {'content_list': ShortenedsUrl.objects.all()})
        else:
            return HttpResponseBadRequest('Invalid Request. url parameter not valid. Probably already exists. Check '
                                          'the list.')
    else:
        return HttpResponseBadRequest('Invalid Request. short parameter already exists')


def treat_short(short):
    exist = ShortenedsUrl.objects.filter(short=short).exists()
    if exist:
        return None, None, exist
    else:
        if short == '':
            print(f"short: {short}")
            urls = ShortenedsUrl.objects.filter(manual_added=False)
            if len(urls) > 0:
                valor_max_short = None
                for obj in urls:
                    if valor_max_short is None or obj.short > valor_max_short:
                        valor_max_short = obj.short
                short = int(valor_max_short) + 1
            else:
                short = 1
            manual = False
        else:
            manual = True
        return short, manual, exist


def treat_url(url):
    if url != '':
        if url.startswith('http://') or url.startswith('https://'):
            return url, True
        else:
            url = 'https://' + url
            return url, True
    else:
        return url, False
