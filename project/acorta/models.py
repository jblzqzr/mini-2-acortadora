from django.db import models


# Create your models here.
class ShortenedsUrl(models.Model):
    url = models.CharField(max_length=200)
    short = models.CharField(max_length=15)
    manual_added = models.BooleanField(default=False)

    def __str__(self):
        return self.url
