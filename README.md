# Mini-2-Acortadora

Repositorio de plantilla para la minipráctica 2 "Web acortadora de URLs versión Django". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.
 
La cuenta de superuser es: 

    - user: admin
    - password: admin

La base de datos también ha sido subida al repositorio con los enlaces que yo he probado.

La aplicación web no deja asignar dos recursos iguales a dos webs diferentes. Una web si puede tener dos recursos diferentes,
el generado automáticamente y de manera secuencial, y el generado manualmente a través del formulario.

Controla también que la query string sea la correcta, con los campos url, no puede ser vacio, y short, que puede ser cualquier valor menor a 15 caracteres, debido a la limitación en el modelo de la base de datos.